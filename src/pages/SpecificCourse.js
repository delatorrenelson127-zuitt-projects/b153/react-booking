import { Link, useParams, useHistory } from "react-router-dom";
import { Card, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2"

export default function SpecificCourse() {
  const { user } = useContext(UserContext);
  const { courseId } = useParams();
  const [courseData, setCourseData] = useState({});
  const { name, description, price } = courseData;

  const history = useHistory()

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
      .then((res) => res.json())
      .then((data) => setCourseData(data));
  };

  useEffect(() => fetchData(), []);

  const enroll = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        courseId: courseId
      })
    })
    .then( res => res.json())
    .then(data => {
      console.log(data)
      if(data){
        Swal.fire({
          title: "Successfully Enrolled",
          icon: "success",
          text: `You have successfully enrolled to ${name}` ,
        })

        history.push("/courses")
      }else{
        Swal.fire({
          title: "Enrollment failed",
          icon: "error",
          text: "Please try again",
        })        
      }
    })
  }

  return (
    <>
      <Card className="my-3">
        <Card.Header className="bg-dark text-white text-center pb-0">
          <h4>{name}</h4>
        </Card.Header>
        <Card.Body>
          <Card.Text>{description}</Card.Text>
          <h6>Price: {price}</h6>
        </Card.Body>
        <Card.Footer className="d-grid">
          {user.id !== null ? (
            <Button variant="primary" onClick={enroll}>Enroll</Button>
          ) : (
            <Link className="btn btn-danger btn-block" to="/login">Login to Enroll</Link>
          )}
        </Card.Footer>
      </Card>
    </>
  );
}
