import { useState, useEffect, useContext } from "react";
// import CourseCard from "../components/CourseCard";
import UserView from "../components/UserView";
import AdminView from "../components/AdminView";
import UserContext from "../UserContext";
import { Container } from "react-bootstrap";

export default function Courses() {
  const [coursesData, setCoursesData] = useState([]);

  const { user } = useContext(UserContext);
  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses`)
      .then((res) => res.json())
      .then((data) => {
        //. then has a "self-contained scope"
        // Any code inside of this .then only exists inside of this .then,
        // to solve this problem, we use a state. By settng the new value seen
        setCoursesData(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, [coursesData]);

  // const courses = coursesData.map((course) =>
  //   course.isActive ? (
  //     <CourseCard key={course._id} courseProps={course} />
  //   ) : null
  // );

  return (
    <Container>
      {user.isAdmin ? (
        <AdminView coursesProp={coursesData} fetchData={fetchData} />
      ) : (
        <UserView coursesProp={coursesData} />
      )}
    </Container>
  );
}
