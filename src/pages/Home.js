import { Fragment } from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home() {
  const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunity for everyone, everywhere",
    destination: "/courses",
    label: "Enroll Now",
  };
  return (
    <Fragment>
      <Banner bannerProps={data} />
      <Highlights />
    </Fragment>
  );
}
