import { useState, useEffect, useContext } from "react";
import { Form, Button, Container } from "react-bootstrap";
import { Redirect, useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  const { user } = useContext(UserContext);

  const history = useHistory();

  const registerUser = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: email }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data)
        if (data) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please provide a diferrent email",
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data) {
                Swal.fire({
                  title: "Registration successful",
                  icon: "success",
                  text: "Welcom to Zuitt!",
                });
                history.push("/login"); // redirect to login page
              } else {
                Swal.fire({
                  title: "Registration unsuccessful",
                  icon: "error",
                  text: "Please try again!",
                });
              }
            });
        }
      });
  };

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNo !== "" &&
      mobileNo.length === 11 &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2]);

  if (user.id !== null) {
    Swal.fire({
      title: "You're already Logged In",
      icon: "error",
      text: "Redirect to Courses",
    });
    return <Redirect to="/" />;
  }

  return (
    // Two-way Binding is implemented for each form input so that we can capture what the use is currently typing in a specific input

    // For example, our firstName input start as blank because its value is our firstName state, which is an empty string by default.

    // As the use types in the firstName input, an onChange event occurs. That onChange even'ts target is the firstName input, and the firstName input's value is whatever the user is currently typing

    // So as the use types, our state is changed to copy the value of the input that they are typing in.
    <Container className="mt-3">
      <h3>Register</h3>
      <Form onSubmit={(e) => registerUser(e)} className="my-3">
        <Form.Group controlId="firstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter First Name"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Last Name"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="email">
          <Form.Label>Enter Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="mobileNo">
          <Form.Label>Mobile Number:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Mobile Number"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="password1">
          <Form.Label>Password: </Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter Password"
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="password2">
          <Form.Label>Verify Password: </Form.Label>
          <Form.Control
            type="password"
            placeholder="Verify Password"
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button
            variant="primary"
            type="submit"
            className="mt-3"
            id="subtmitBtn"
          >
            Submit
          </Button>
        ) : (
          <Button
            variant="primary"
            type="submit"
            className="mt-3"
            id="subtmitBtn"
            disabled
          >
            Submit
          </Button>
        )}
      </Form>
    </Container>
  );
}
