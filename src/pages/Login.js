import { useState, useEffect, useContext } from "react";
import { Redirect, useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { Form, Col, Row, Button } from "react-bootstrap";
import UserContext from "../UserContext";

export default function Login() {
  // get the user stat from the user context object
  const { user, setUser } = useContext(UserContext);

  const history = useHistory();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  const loginUser = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data)
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken); //save JSON Web token to local storage

          // retrieve user details
          fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              setUser({
                id: data._id,
                isAdmin: data.isAdmin,
              });

              Swal.fire({
                title: "Login successful",
                icon: "success",
                text: "Welcom to Zuitt!",
              });
              history.push("/courses"); // redirect to login page
            });
        } else {
          Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login details and try again",
          });
        }
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  if (user.id !== null) {
    Swal.fire({
      title: "You're already Logged In",
      icon: "error",
      text: "Redirect to Courses",
    });
    return <Redirect to="/" />;
  }

  return (
    <Row className="justify-content-md-center mt-4">
      <Col md={4}>
        <Form onSubmit={(e) => loginUser(e)} className="my-3">
          <h3>Login</h3>
          <Form.Group controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group controlId="password" className="mt-4">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          {isActive ? (
            <Button
              variant="primary"
              type="submit"
              className="mt-3"
              id="subtmitBtn"
            >
              Login
            </Button>
          ) : (
            <Button
              variant="secondary"
              type="submit"
              className="mt-3"
              id="subtmitBtn"
              disabled
            >
              Login
            </Button>
          )}
        </Form>
      </Col>
    </Row>
  );
}
