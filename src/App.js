import { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import SpecificCourse from "./pages/SpecificCourse";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Error from "./pages/Error";
import { UserProvider } from "./UserContext";
import "./App.css";

// App.js is typically called the "top-level component"

function App() {
  // main app-wide user state
  // the user state determines whether a user is logged in or not (via the id) and if the user is an Admin (via isAdmin)
  const [user, setUser] = useState({ id: null, isAdmin: null });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null,
    });
  };

  // Because our user stat's values are reset to null hwen the page reloads
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // if we received the user's data from our API, set the user states values back to the user's details
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          // if not set the user state's to null
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  });

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/home" component={Home} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/courses/:courseId" component={SpecificCourse} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route component={Error} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}

/*
  Delete ff:
    index.css
    reportWebVitals.js
    logo.svg
    App.test.js
*/

export default App;
