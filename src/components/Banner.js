import {Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Banner({bannerProps}){
    const {title, content, destination, label} = bannerProps;

    return(
        <Row className="p-5 text-center">
            <Col>
                <h1>{title}</h1>
                <p>{content}</p>
                {/* <Button variant="primary">{label}</Button> */}
                <Link className="btn btn-primary" to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}