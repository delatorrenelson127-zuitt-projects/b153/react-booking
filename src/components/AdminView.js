import { useState, useEffect } from "react";
import CourseCard from "./CourseCard";
import { Table, Button, Modal, Form } from "react-bootstrap";
import Swal from "sweetalert2";

// Activity console.log course date here
// but do fetch request somewhere else
export default function AdminView(props) {
  const [name, setName] = useState("")
  const [description, setDescription] = useState("")
  const [price, setPrice] = useState(0)
  const [isActive, setIsActive] = useState(false)


  const { coursesProp, fetchData } = props;

  const [showAdd, setShowAdd] = useState(false)
  const [showEdit, setShowEdit] = useState(false)

  

  // console.log(coursesProp)
  // console.log(fetchData)

  const [coursesArr, setCoursesArr] = useState([]);

  const token = localStorage.getItem("token");

  const openAdd = () => setShowAdd(true)
  const closeAdd = () => setShowAdd(false)

  const openEdit = () => setShowEdit(true)
  const closeEdit = () => setShowEdit(false)

  const archiveToggle = (courseId, isActive) => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        isActive: !isActive,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data)
        let bool = data ? "disabled" : "re-enabled";

        fetchData();

        Swal.fire({
          title: "Succes",
          icon: "success",
          text: `Course successfully ${bool}`,
        });
      });
  };

  const addCourse = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/courses`,{
      method: "POST",
      headers:{
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data){
        Swal.fire({
          title: "Succesfully Add New Course",
          icon: "success",
          text: `Course successfully added`,
        });
        fetchData()
        closeAdd()
      }else{
        Swal.fire({
          title: "Succesfully Add New Course",
          icon: "success",
          text: `Course successfully added`,
        });
        fetchData()        
      }
    })
  }

  useEffect(() => {
    if (
      name !== "" &&
      description !== "" &&
      price !== "" 
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }

    const courses = coursesProp.map((course) => {
      return (
        <tr key={course._id}>
          <td>{course.name}</td>
          <td>{course.description}</td>
          <td>{course.price}</td>
          <td>
            {course.isActive ? (
              <span>AvaiLabel</span>
            ) : (
              <span>UnavaiLabel</span>
            )}
          </td>
          <td>
            <Button variant="primary" className="m-2" size="sm" onClick={openEdit}>
              Update
            </Button>
            {course.isActive ? (
              <Button
                variant="danger"
                onClick={() => archiveToggle(course._id, course.isActive)}
              >
                Disabled
              </Button>
            ) : (
              <Button
                variant="success"
                onClick={() => archiveToggle(course._id, course.isActive)}
              >
                Enable
              </Button>
            )}
          </td>
        </tr>
      );
    });
    setCoursesArr(courses);
  }, [coursesProp]);




  return (
    <>
      <div className="my-4">
        <h2>Admin Dashboard</h2>
        <Button variant="primary" onClick={openAdd}>Add Course</Button>
      </div>

      <Table striped bordered hover responsive>
        <thead className="bg-dark text-white">
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{coursesArr}</tbody>
      </Table>
      {/*Add Course Modal */}
      <Modal show={showAdd} onHide={closeAdd}>
        <Form onSubmit={e => addCourse(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add Course</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form.Group controlId="name_add">
                <Form.Label>Name</Form.Label>
                <Form.Control type="text"  onChange ={e => {setName(e.target.value)}}/>
              </Form.Group>
              <Form.Group controlId="description_add">
                <Form.Label>Description</Form.Label>
                <Form.Control type="text" onChange ={e => {setDescription(e.target.value)}}/>
              </Form.Group>
              <Form.Group controlId="price_add">
                <Form.Label>Price</Form.Label>
                <Form.Control type="number"  onChange ={e => {setPrice(e.target.value)}}/>
              </Form.Group>
            </Modal.Body>          
          <Modal.Footer>
            <Button variant="secundary" onClick={closeAdd}>Close</Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>      
      {/*End Add Course Modal */}

      {/* Edit course modal */}
      <Modal show={showEdit} onHide={closeEdit}>
        <Form>
          <Modal.Header closeButton>
            <Modal.Title>Update Course</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form.Group controlId="name">
                <Form.Label>Name</Form.Label>
                <Form.Control type="text"/>
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control type="text"/>
              </Form.Group>
              <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control type="number"/>
              </Form.Group>
            </Modal.Body>          
          <Modal.Footer>
            <Button variant="secundary" onClick={closeEdit}>Close</Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      {/* End Edit course modal */}
    </>
  );
}
