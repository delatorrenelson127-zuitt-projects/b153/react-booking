import { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

export default function AppNavbar() {
  // analogy:
  // The user state and user state functions (setUser/unsetUser) that we defined in App.js are our "gifts"
  // When UserProvider
  const { user, unsetUser } = useContext(UserContext);

  const history = useHistory();

  const logout = () => {
    unsetUser();
    history.push("/login"); // redirect to login page
  };

  // conditional rendering for our NavBar, showing "Register" and "Login" for user who are not logged in,
  // and "Lot Out" for users who are logged in and

  const rightNav = !user.id ? (
    <>
      <Link className="nav-link" to="/register">
        Register
      </Link>
      <Link className="nav-link" to="/login">
        Log In
      </Link>
    </>
  ) : (
    <Link className="nav-link" to="/" onClick={logout}>
      Log Out
    </Link>
  );

  return (
    <Navbar bg="light" expand="lg" className="mb-3">
      <Container>
        <Link className="navbar-brand" to="/home">
          Zuitt
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Link className="nav-link" to="/home">
              Home
            </Link>
            <Link className="nav-link" to="/courses">
              Courses
            </Link>
            {rightNav}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
