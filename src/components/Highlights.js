import { Card, Col, Row } from "react-bootstrap";

export default function Highlights() {
  return (
    <Row>
      <Col xs={12} md={4} className="mb-3">
        <Card className="cardHighlight p-3 h-100" data-aos='flip-left'>
          <Card.Body>
            <Card.Title className="text-center">
              <h2>Learn From Home</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Quisquam, nemo voluptates. Quod, ipsa illo. Atque harum quae sunt
              totam sed? Numquam tempore quo expedita ipsam explicabo ex est
              facere quia!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mb-3">
        <Card className="cardHighlight p-3 h-100" data-aos='flip-left'>
          <Card.Body>
            <Card.Title className="text-center">
              <h2>Study Now Pay Later</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Quisquam, nemo voluptates. Quod, ipsa illo. Atque harum quae sunt
              totam sed? Numquam tempore quo expedita ipsam explicabo ex est
              facere quia!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mb-3">
        <Card className="cardHighlight p-3 h-100" data-aos='flip-left'>
          <Card.Body>
            <Card.Title className="text-center">
              <h2>Be Part of Our Community</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Quisquam, nemo voluptates. Quod, ipsa illo. Atque harum quae sunt
              totam sed? Numquam tempore quo expedita ipsam explicabo ex est
              facere quia!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
