
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";

export default function CourseCard({ courseProps }) {
  const { _id, name, description, price, isActive } = courseProps;

  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  // const [isOpen, setIsOpen] = useState(true)

  // useEffect allows us to execute code when a given state (inside of the array) changes

  // If the array is emty, useEffect will Only hapen on component mount AND when the state changes (when it first loads)

  // If there are two or more states in the array, useEffect will fire when ANY of those states change
  // useEffect(()=>{
  //   if(seats === 0){
  //     setIsOpen(false)
  //   }
  // },[seats, count])

  // const enroll = () => {
  //   if (seats <= 0) {
  //     // setIsOpen(false) use useEffect instead
  //     return false;
  //   }

  //   setCount(count + 1);
  //   setSeats(seats - 1);
  // };

  return (
    <Card
      key={_id}
      className="my-3 cardHighlight p-3 h-100"
      data-aos="flip-left"
    >
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>
          <strong>Description:</strong> {description}
        </Card.Text>
        <Card.Text>
          <strong>Price:</strong> {price.toLocaleString()}
        </Card.Text>
        <Card.Text>{/* <strong>Enrollees: </strong> {count} */}</Card.Text>

        {isActive ? (
          <>
            {/* <Button variant="primary" onClick={enroll}>
          Enroll
        </Button>  */}
            <Link
              className="btn btn-primary mx-2"
              to={`/courses/${_id}`}
              variant={"primary"}
            >
              Details
            </Link>
          </>
        ) : (
          <>
            {/* <Button disabled variant="primary" onClick={enroll}>
            Enroll
          </Button>  */}
            <Link
              className="btn btn-primary mx-2"
              to={`/courses/${_id}`}
              disabled={"true"}
              variant={"primary"}
            >
              Details
            </Link>
          </>
        )}
      </Card.Body>
    </Card>
  );
}
