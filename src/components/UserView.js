import { useState, useEffect } from "react";
import CourseCard from "./CourseCard";

export default function UserView({coursesProp}) {
  const [coursesArr, setCoursesArr] = useState([]);

  useEffect(() => {
    const courses = coursesProp.map((course) =>
      course.isActive ? <CourseCard key={course._id} courseProps={course} /> : null)
    setCoursesArr(courses)
  }, []);

  return <>{coursesArr}</>;
}
