import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />    
  </React.StrictMode>,
  document.getElementById('root')
);

// const name = "Tony Stark";
// const user = {
//   firstName: "Thor",
//   lastname: "Odinson"
// }
// const formatName = user => `${user.firstName} ${user.lastname}`
// const element  = <h1>Hello, {formatName(user)}</h1>

// ReactDOM.render(
//   element,
//   document.getElementById("root")
// )
